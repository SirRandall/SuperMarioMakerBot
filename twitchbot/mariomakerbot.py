#!/usr/bin/python3
from twitchio import commands as tcommands
import datetime
import socket


class Botto(tcommands.TwitchBot):
    """Create our IRC Twitch Bot.
    api_token is optional, but without it, you will not be able to make certain calls to the API."""

    def __init__(self):
        super().__init__(prefix=['!', '?'], token='oauth:', api_token='oauth:', client_id='clientid', nick='Tenebrae', initial_channels=['sirrandallgaming'])
        #NICK = "TenebraeB0T"            # Twitch username your using for your bot
        #PASS = "oauth:kylcn6gclncughi2p8lg2srhz89ift"

    print("hello")
    async def event_ready(self):
        """Event called when the bot is ready to go!"""
        print('READY!')

    '''async def event_message(self, message):
        """Event called when a message is sent to a channel you are in."""
        if message.content == 'Hello':
            await message.send('World!')'''

    async def event_message(self, message):
        fdate = datetime.date.today()
        if message.content.lower() == 'hello':
            await message.send('Hello {}'.format(message.author.name))
        with open('logs/chatlog-{}'.format(fdate), 'a+') as f:
            f.write("{},{},{}\n".format(datetime.datetime.now().ctime(), message.author.name, message.content))
            f.close()
        host = socket.gethostname()
        port = 12345                   # The same port as used by the server
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        srvmes = (message.author.name + ": " + message.content)
        s.sendall(srvmes.encode("utf-8"))
        s.close()

    async def event_join(self, user):
        print(user.name + " has joined")
        await user.send('Welcome, Please Follow the rules.')
    
    '''async def event_raw_data(self, data):
        print(data)'''

    @tcommands.twitch_command(aliases=['silly'])
    async def silly_command(self, ctx):
        """A simple command, which sends a message back to the channel!"""
        await ctx.send('Hai there {0} Kappa.'.format(ctx.author.name))

    @tcommands.twitch_command(aliases=['tc'])
    async def tournament_code(self,ctx):
        """Torny Code"""
        await ctx.send('Tournament Code is - 0526-9011-2043')
        
    @tcommands.twitch_command(aliases=['dc'])
    async def discord_command(self, ctx):
        """Discord command"""
        await ctx.send('https://discord.gg/x2DeRzB')

    @tcommands.twitch_command(aliases=['fc'])
    async def friendcode(self, ctx):
        """Switch Friend Code"""
        await ctx.send('sw-2753-5481-7222')

    @tcommands.twitch_command()
    async def date(self, ctx):
        """Date"""
        await ctx.send('Today is {}'.format(datetime.date.today()))

bot = Botto()
bot.run()
print("hello")
