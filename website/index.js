const request = require('request')
const fs = require("fs");
var queue = require("./queue.json");

exports.get = function(url, headers, callback) {
	var options = {
		url: url,
		method: 'GET',
		headers: headers
	}
	request(options, function (error, response, body) {
		if (!error && response.statusCode == '200') {
			try {
				return callback(JSON.parse(body))
			}
			catch(error) {
				return callback(body)
			}
		} else {
            return callback(response.statusCode)
        }
	})

}

exports.post = function(url, data, headers, callback) {
	var options = {
		url: url,
		method: 'POST',
		headers: headers,
		form: data
	}
	request(options, function (error, response, body) {
		if (!error && response.statusCode == '200') {
			try {
				return callback(JSON.parse(body))
			}
			catch(error) {
				return callback(body)
			}
		} else {
            return response.statusCode
        }
	})
}

exports.server = function () {
    let base_url = "http://localhost:3000"
    const express = require('express')
    const auth_key = "super_secret_password"
    const app = express()
    app.set('views', './templates')
    app.set('view engine', 'pug')

    const bodyParser = require('body-parser')

    app.use(bodyParser.urlencoded({
        extended: false
    }))     

    app.use(bodyParser.json())
    const port = 3000

    app.get('/current', (request, response) => {
        response.send('Work in progress')
    })    

    app.post('/next', (request, response) => {
        if (request.body.auth_key == undefined || request.body.auth_key != auth_key) {
            response.status(401)
            response.send(JSON.stringify({ error: "authentication" }))
            return
        }
        level = queue.items.shift()
        response.send(level)
    })

    app.get('/queue', (request, response) => {
        response.send(queue)
    })

    app.post('/add', (request, response) => {
        if (request.body.auth_key == undefined || request.body.auth_key != auth_key) {
            response.status(401)
            response.send(JSON.stringify({ error: "authentication" }))
            return
        } else if (request.body.username == undefined) {
            response.status(400)
            response.send(JSON.stringify({ error: "parameters" }))
            return
        } else if (request.body.level_id == undefined) {
            response.status(400)
            response.send(JSON.stringify({ error: "parameters" }))
            return
        } else if (request.body.time == undefined) {
            response.status(400)
            response.send(JSON.stringify({ error: "parameters" }))
            return
        }

        exports.get('https://supermariomakerbookmark.nintendo.net/courses/' + request.body.level_id,  {'User-Agent': 'Super Agent/0.0.1'}, function(course_response){
            if (course_response == '404') {
                response.status(400)
                response.send(JSON.stringify({ error: "level" }))
                return
            }
    
            queue.items.forEach(function(item) {
                if (item.level_id == request.body.level_id) {
                    response.status(400)
                    response.send(JSON.stringify({ error: "duplicate" }))
                    return
                }
            })
    
            new_level = {
                'username'  : request.body.username,
                'level_id'  : request.body.level_id,
                'time'      : request.body.time,
            }
    
            queue.items.push(new_level)
            var place = queue.items.length
    
            fs.writeFile( "queue.json", JSON.stringify( queue ), "utf8", function () {})
    
            response.send(String(place))
        })
    })

    app.listen(port, (err) => {
      if (err) {
        return console.log('something bad happened', err)
      }
  
      console.log(`server is listening on ${port}`)
    })
  }

exports.server()