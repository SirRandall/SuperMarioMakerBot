#!/usr/bin/env python

from time import sleep
import requests
from youtubechat import YoutubeLiveChat, get_live_chat_id_for_stream_now, LiveChatApi
from bs4 import BeautifulSoup
import urllib.request as urllib2

livechat_id = get_live_chat_id_for_stream_now("oauth_creds")
#chat_obj = YoutubeLiveChat([livechat_id])
chat_obj = YoutubeLiveChat("oauth_creds", [livechat_id])
print(chat_obj)
#chatobj = LiveChatApi([livechat_id])
def respond(msgs, chatid):
	chat_obj.send_message(f"{msgs[0].author.display_name} Checking LevelID Please wait", chatid)
    
	for msg in msgs:
	    print(msg)
	    print(msgs)
	    print(chatid)
	    #msg.delete()
	    if str(msg).startswith('!add'):
		    #chat_obj.send_message(f"{msg.author.display_name} Checking LevelID Please wait", chatid)
		    command='!add'
		    text = str(msg)
		    message = text.split(" ", 1)
		    level = message[1].upper()
		    url = f'https://supermariomakerbookmark.nintendo.net/courses/{level}'
		    check = requests.get(f'{url}')
		    '''page = urllib2.urlopen(url)
		    soup = BeautifulSoup(page, 'html.parser')
		    name_box = soup.find('div', attrs={'class': 'course-title'})
		    name = name_box.text.strip()'''
		    if int(check.status_code) == 200:
			    page = urllib2.urlopen(url)
			    soup = BeautifulSoup(page, 'html.parser')
			    name_box = soup.find('div', attrs={'class': 'course-title'})
			    name = name_box.text.strip()
			    chat_obj.send_message(f"{msg.author.display_name} your level \n {name}  \n has been added", chatid)
		    else:
			    chat_obj.send_message(f"{msg.author.display_name} level not found or invalid format. Check the level id and make sure to use hyphens Example: !add EEBD-0000-01CE-4CB6", chatid)
            


try:
    #chat_obj.live_chat_messages_insert("Hello")
    chat_obj.start()
    #chat_obj.live_chat_messages_insert("Hello")    
    chat_obj.subscribe_chat_message(respond)
    chat_obj.join()
    #chat_obj.livechat_api.live_chat_messages_insert("Hello")
    #chat_obj.subscribe_chat_message(respond)
    chat_obj.send_message("hello",'EiEKGFVDZi1HQWV6RTBLWmRhNndkY3ZRWmpCURIFL2xpdmU') 
finally:
    chat_obj.stop()
